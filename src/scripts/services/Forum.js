
angular.module('Forum')
	.factory('$forumservice',['$resource','$cookieStore',function($resource){
		$resource.cat_id=null;
		return {
			setID:function(id){
				$resource.cat_id=id;
			
			},
			getID:function(){
				return $resource.cat_id;
			},
			category:$resource('http://localhost:8000/api/categories/:id/?', {id:'@_id'},{update: {
      			method: 'PUT',
      			
   		 		}
			}),
			post:$resource('http://localhost:8000/api/posts/:id/?',{id:'@_id'},{update: {
      			method: 'PUT'
    			}
			}),
			comment:$resource('http://localhost:8000/api/comments/:id/?',{id:'@_id'},{update: {
      			method: 'PUT'
    			}
			})
	}

	}
	
]);