angular.module('Forum').factory("auth", function($resource,$http, $q, $window,$cookieStore,$rootScope) {
  
  function login(username, password,callback) {
    var deferred = $q.defer();
 
    $http.post("http://localhost:8000/api/login/", {
      username: username,
      password: password
    }).then(function(result) {
    	callback(result);
      
      $cookieStore.put('token',result.data.token);
      $rootScope.username=username;
    }, function(error) {
      
      callback(error);
    });
 
   
  }

 
  return {
    login: login,
    users:$resource('http://localhost:8000/api/users/:id/?',{id:'@_id'},{update: {
      			method: 'PUT'
    			}
			})
    
  };
});