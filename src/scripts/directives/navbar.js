'use strict';


angular.module('Forum')
 .directive('navbar',
	function () {
		return {
			restrict: 'E',
			link: function ($scope, $element, $attrs) {
				$scope.navbarElements = [
				{name:'Home',url:'#/home'},
				{name:'Categories',url:'#/categories'},
				{name:'Create Categories',url:'#/categories/add/'},
				{name:'Posts',url:'#/posts'},
				{name:'Log In',url:'#/login'},
				{name:'Register',url:'#/register/'},
				{name:'Log out',url:'#/logout/'}

				];
				$scope.brand = 'Egemsoft';
			},
			templateUrl: '/src/views/navbar.html',
			controller: function ($scope) {

			}
		};
	});