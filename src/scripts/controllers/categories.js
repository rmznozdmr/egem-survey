'use strict';

angular.module('Forum')
	.controller('QuestionListCtrl',['$scope','$forumservice','$route','$routeParams',function($scope,$forumservice,$route,$routeParams){
		
		$scope.getID=function(id) {
			// body...
			$forumservice.setID(id);
			$routeParams.id=id;
		}
		$scope.categories = $forumservice.category.query();
		$scope.deleteCategory = function(id){
			$forumservice.category.remove({id:id});
			$route.reload();
		};
		

		
		
	}]);