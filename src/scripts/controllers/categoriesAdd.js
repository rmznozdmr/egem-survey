'use strict';

angular.module('Forum')
.controller('CatAddCtrl',['$scope','$forumservice',function($scope,$forumservice){
	$scope.category = null;
	
	$scope.saveCategory = function(){
		$forumservice.category.save($scope.category);
		window.location='#/categories';
	};
	
}]);