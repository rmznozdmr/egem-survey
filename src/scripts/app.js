'use strict';

angular.module('Forum',['ngRoute','ngResource','ngCookies'])
	.config(function ($routeProvider) {
			$routeProvider
				.when('/home', {
					templateUrl: '/src/views/home.html',
					controller: 'HomeCtrl'
					
				})
				 .when('/login', {
      				templateUrl: '/src/views/auth.html',
      				controller: 'AuthCtrl'
    			})
				.when('/register/', {
					templateUrl: '/src/views/register.html',
					controller: 'UserAddCtrl'
				})
				.when('/categories/', {
					templateUrl: '/src/views/categoriesList.html',
					controller: 'QuestionListCtrl'
				})
				.when('/posts/', {
					templateUrl: '/src/views/postsList.html',
					controller: 'PostListCtrl'
				})
				.when('/categories/add', {
					templateUrl: '/src/views/categoriesAdd.html',
					controller: 'CatAddCtrl'
				})
				.when('/postsAdd/:id', {
					templateUrl: '/src/views/postAdd.html',
					controller: 'PostAddCtrl'
				})
				.when('/topicUpdate/:id', {
					templateUrl: '/src/views/topicUpdate.html',
					controller: 'topicUpdateCtrl'
				})
				.when('/categoryDetay/:id', {
					templateUrl: '/src/views/catDetail.html',
					controller: 'CatDetailCtrl'
				})
				.when('/categoryUpdate/:id', {
					templateUrl: '/src/views/catUpdate.html',
					controller: 'CatUpdateCtrl'
				})
				.when('/logout/', {
					templateUrl: '/src/views/logout.html',
					controller: 'LogOutCtrl'
				})
				.otherwise({
					redirectTo: '/home'
				});
		})
		.config(function ($httpProvider) {
			$httpProvider.defaults.useXDomain = true;
			$httpProvider.defaults.xsrfCookieName = 'csrftoken';
			$httpProvider.defaults.xsrfHeaderName = 'X-CSRFToken';
		});
		run.$inject = ['$rootScope', '$location', '$cookieStore', '$http'];
    	function run($rootScope, $location, $cookieStore, $http) {
        // keep user logged in after page refresh
        $rootScope.globals = $cookieStore.get('globals') || {};
        if ($rootScope.globals.currentUser) {
            $http.defaults.headers.common['Authorization'] = 'Basic ' + $rootScope.globals.currentUser.authdata; // jshint ignore:line
        }

        $rootScope.$on('$locationChangeStart', function (event, next, current) {
            // redirect to login page if not logged in and trying to access a restricted page
            var restrictedPage = $.inArray($location.path(), ['/login', '/register']) === -1;
            var loggedIn = $rootScope.globals.currentUser;
            if (restrictedPage && !loggedIn) {
                $location.path('/login');
            }
        });
    }